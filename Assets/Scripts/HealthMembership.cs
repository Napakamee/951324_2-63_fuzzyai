﻿using System.Collections;
using System.Collections.Generic;
using Fuzzy_Logic_Sugeno;
using UnityEngine;

public class HealthMembership
{
    private FuzzyLogic fuzzyLogic = new FuzzyLogic();

    double c0 = 20;
    double c1 = 40;
    double c2 = 50;
    double c3 = 60;
    double c4 = 80;
    double c5 = 90;
    
    
    public HealthMembership()
    {


    }
    
    public double calMeberShipValueNearDeath(double x)
    {
        double membervalue = 0;

        membervalue = fuzzyLogic.FuzzyReverseGrade(x, 20, c1);

        return membervalue;
    }

    public double calMeberShipValueMediumHP(double x)
    {
        double membervalue = 0;

        membervalue = fuzzyLogic.FuzzyTriangle(x, c0, c2, c4);

        return membervalue;
    }
    public double calMeberShipValueHighHP(double x)
    {
        double membervalue = 0;

        membervalue = fuzzyLogic.FuzzyGrade(x, c3, c5);

        return membervalue;
    }
}
