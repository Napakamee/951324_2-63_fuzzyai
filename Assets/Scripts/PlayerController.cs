﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    private float inputX = 0;
    private float inputY = 0;
    [SerializeField] private float inputFire = 0;
    private bool isFiring = false;
    private float nextFire = 0.0f;
    [SerializeField] private Rigidbody2D rb = null;
    public GameObject bullet;
    public Transform firePoint;
    private Vector3 _movement;
    
    [SerializeField] private GameObject explode;
    [SerializeField] private GameObject bigExplode;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
        
        if (inputFire > 0)
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + GameManager.Instance.fireRate;
                Shoot();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            ContactPoint2D contact = other.contacts[0];
            Vector3 pos = contact.point;
            
            float damage = other.gameObject.GetComponent<EnemyDamage>().damage;
            GameManager.Instance.playerHp -= damage;
            
            GameObject expl = Instantiate(explode, pos, Quaternion.identity) as GameObject;
            Destroy(expl, .5f);
            if (GameManager.Instance.playerHp <= 0)
            {
                GameObject bigExpl = Instantiate(bigExplode, pos, Quaternion.identity) as GameObject;
                Destroy(bigExpl, 1f);
                GameManager.Instance.isStartGame = false;
                GameManager.Instance.gameState = GameManager.LOSE;
                Destroy(this.gameObject);
            }
            
            Destroy(other.gameObject);
        }
    }

    private void Shoot()
    {
        GameObject bl = Instantiate(bullet, firePoint.position, firePoint.rotation);
        Rigidbody2D rb_bl = bl.GetComponent<Rigidbody2D>();

        rb_bl.AddForce(firePoint.up * GameManager.Instance.bulletForce, ForceMode2D.Impulse);
        //this.GetComponent<AudioSource>().PlayOneShot(fireSound);
    }

    private void ProcessInput()
    {
        inputX = Input.GetAxisRaw("Horizontal");
        inputY = Input.GetAxisRaw("Vertical");
        inputFire = Input.GetAxisRaw("Fire1");
        _movement = new Vector3(inputX, inputY, 0).normalized;
        Move();
    }
    private void Move()
    {
        rb.velocity = new Vector3(_movement.x * speed, _movement.y * speed);
    }
}
