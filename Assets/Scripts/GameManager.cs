﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    public const int WIN = 1;
    public const int LOSE = 0;
    
    public float playerHp = 100;
    public float maxHp = 100;
    public Slider myHealthBar;
    
    public float fireRate = 0.2f;
    public float bulletForce = 2f;
    public float bulletDamage = 1f;

    public bool isStartGame = false;
    public int gameState = -1;

    private AIScript _aiScript;

    public GameObject LossPanel;
    public GameObject WinPanel;
    void Awake()
    {
        if (Instance == null)
            Instance = this;

    }
    void Start()
    {
        if (myHealthBar != null)
        {
            myHealthBar.maxValue = maxHp;
        }
        isStartGame = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (myHealthBar != null)
        {
            myHealthBar.value = playerHp;
        }

        if (!isStartGame)
        {
            switch (gameState)
            {
                case WIN: GameOver(gameState);
                    WinPanel.SetActive(true);
                    break;
                case LOSE: GameOver(gameState);
                    LossPanel.SetActive(true);
                    break;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        
    }

    public void GameOver(int State)
    {

    }
}
