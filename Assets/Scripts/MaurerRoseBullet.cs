﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaurerRoseBullet : MonoBehaviour
{
    const int PointsCount = 361;

    [SerializeField]
    float N = 2;

    [SerializeField]
    float D = 29;

    [SerializeField]
    float Scale = 5;

    [SerializeField] private GameObject bullet;
    
    Vector3[] _points;
    float _k;
    float _r;
    float _x;
    float _y;
    Vector3 _previousPosition;
    float _previousN;
    float _previousD;
    float _previousSize;
    // Start is called before the first frame update
    void Start()
    {
        _points = new Vector3[PointsCount];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SpawnMaurerRoseBullet()
    {
        if (_previousPosition != transform.position
            || _previousD != D
            || _previousN != N
            || _previousSize != Scale)
        {
            Spawn();
            _previousPosition = transform.position;
            _previousD = D;
            _previousN = N;
            _previousSize = Scale;
        }
        
    }

    private void Spawn()
    {
        for (int i = 0; i < 5; i++)
        {
            var position = transform.position;
            
            _k = i * (D * Mathf.PI / 180f);
            _r = Scale * Mathf.Sin(N * _k);
            _x = _r * Mathf.Cos(_k);
            _y = _r * Mathf.Sin(_k);
            _points[i] = transform.position + new Vector3(_x, _y, 0);
            Vector3 bulMoveVector = new Vector3(_x,_y,0f);
            Vector2 bulDir = (bulMoveVector - position).normalized;
            
            GameObject b = Instantiate(bullet) as GameObject;
            b.transform.position = position;
            b.transform.rotation = transform.rotation;
            b.GetComponent<EnemyBullet>().SetMoveDirection(bulDir);
            b.GetComponent<EnemyBullet>().moveSpeed = 2f;
        }
    }
}
