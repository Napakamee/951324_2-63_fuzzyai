﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fuzzy_Logic_Sugeno;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AIScript : MonoBehaviour
{
    const int IDLE = 0;
    const int SPLASH = 1;
    const int SPIRAL = 2;
    const int FLEE = 3;
    const int DIE = 4;

    private Vector2 screenBounds;
    [SerializeField] private GameObject explode;
    [SerializeField] private GameObject bigExplode;

    #region UIVar
    [Header("UI")]
    private int state;
    [SerializeField] private TextMeshProUGUI outputTxt;
    [SerializeField] private TextMeshProUGUI stateTxt;
    [SerializeField] private TextMeshProUGUI degreeTxt;
    [SerializeField] private Slider hpBar;
    public string stringState = "";
    
    #endregion
    
    #region MovementVar
    [Header("Movement")]
    [SerializeField] Transform player;
    [SerializeField] private Transform[] wayPoint;
    [SerializeField] private int nextWayPoint = 0;

    private Rigidbody2D rb;
    private FuzzyLogic _fuzzyLogic = new FuzzyLogic();
    private HealthMembership playerHealthMembership = new HealthMembership();
    private HealthMembership aIHealthMembership = new HealthMembership();
    

    #endregion
    

    [Header("Stat")]
    public double hp = 100;
    public float speed = 1.0f;
    private double playerHp = 100;
    
    public double output = 0;

    [Header("Splash Values")] 
    [SerializeField] private GameObject bullet;
    [SerializeField] float startAngle = 90f, endAngle = 270f;
    [SerializeField] int bulletAmount = 10;
    public float firerate = 2.0f;
    
    [Header("SpiralValue")]
    private float angle = 0f;
    private Vector2 bulletMoveDirection;
    public float delay = 0.05f;
    [SerializeField]private float nextShot = 2;

    [Header("Minion")] 
    [SerializeField] private GameObject minion;
    
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(EnemyFire());
        rb = this.GetComponent<Rigidbody2D>();
        if (GameManager.Instance.isStartGame)
            player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    IEnumerator EnemyFire()
    {
        while (true)
        {
            float tempFirerate = 0;
            if (output >= 0)
            {
                tempFirerate = (float) ((firerate / 20) * (3.5 + output));
            }
            else if (output < 0)
            {
                tempFirerate = (float) ((firerate / 8) * (3.5 + -output));
            }
            Debug.Log(tempFirerate);
            yield return new WaitForSeconds(tempFirerate);

            switch (state)
            {
                case SPLASH: bulletAmount = 10; SpawnSpreadBullet(); break;
                //case SPIRAL: SpawnSpiralBullet(); break;
                case FLEE: SpawnMinion(); bulletAmount = 6; SpawnSpreadBullet(); break;
            }
        }
    }
    // Update is called once per frame
    public void Update()
    {
        if (GameManager.Instance.isStartGame)
        {
            this.playerHp = GameManager.Instance.playerHp;
            Transition();
            stateTxt.text = "State: " + stringState;
            outputTxt.text = "Output " + output;
            hpBar.value = (float) hp;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            ContactPoint2D contact = other.contacts[0];
            Vector3 pos = contact.point;
            this.hp -= GameManager.Instance.bulletDamage;
            GameObject expl = Instantiate(explode, pos, Quaternion.identity) as GameObject;
            Destroy(expl, .5f);
            Destroy(other.gameObject);
            if (hp <= 0)
            {
                GameObject bigExpl = Instantiate(bigExplode, pos, Quaternion.identity) as GameObject;
                Destroy(bigExpl, 1f);
                Destroy(this.gameObject);
                GameManager.Instance.isStartGame = false;
                GameManager.Instance.gameState = GameManager.WIN;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
    }

    private void Transition()
    {
        
        
        double degreeHighHP = aIHealthMembership.calMeberShipValueHighHP(hp);
        double degreeMediumHP = aIHealthMembership.calMeberShipValueMediumHP(hp);
        double degreeLowHP = aIHealthMembership.calMeberShipValueNearDeath(hp);

        double degreePlayerHighHP = playerHealthMembership.calMeberShipValueHighHP(playerHp);
        double degreePlayerMediumHP = playerHealthMembership.calMeberShipValueMediumHP(playerHp);
        double degreePlayerLowHP = playerHealthMembership.calMeberShipValueNearDeath(playerHp);

        //AIHigh AND PlayerMedium then Chasing
        //double degreeChasing = _fuzzyLogic.FuzzyAND(degreeHighHP, degreePlayerMediumHP);
        double degreeChasing = _fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyAND(degreeHighHP, degreePlayerHighHP),
            _fuzzyLogic.FuzzyAND(degreeHighHP, degreePlayerMediumHP)),
            _fuzzyLogic.FuzzyAND(degreeMediumHP,degreePlayerLowHP));
        
        double degreeNatural = _fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyAND(degreeMediumHP, degreePlayerMediumHP),
            _fuzzyLogic.FuzzyAND(degreeMediumHP, degreePlayerHighHP)),
            _fuzzyLogic.FuzzyAND(degreeHighHP, degreePlayerLowHP));
        
        double degreeFlee = _fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyOR(_fuzzyLogic.FuzzyAND(degreeLowHP, degreePlayerHighHP),
            _fuzzyLogic.FuzzyAND(degreeLowHP, degreePlayerMediumHP)),
            _fuzzyLogic.FuzzyAND(degreeLowHP, degreePlayerLowHP));

        
        //Debug.Log("DegreeChasing: " + degreeChasing);
        //Debug.Log("DegreeNatural: " + degreeNatural);
        //Debug.Log("DegreeFlee: " + degreeFlee);
        output = (-3 * degreeFlee + 1 * degreeNatural + 3 * degreeChasing) /
                 (degreeFlee + degreeNatural + degreeChasing);
        if (output > 1)
        {
            state = SPLASH;
            stringState = "Splash";
        }
        else if (output <= 1 && output > 0)
        {
            state = SPIRAL;
            stringState = "Spiral";
        }
        else
        {
            state = FLEE;
            stringState = "Flee";
        }
        ChangeState(state);
        degreeTxt.text = "HighHP: " + degreeHighHP + " MediumHP: " + degreeMediumHP + " LowHP: " + degreeLowHP;
    }

    public void ChangeState(int currentState)
    {
        switch (currentState)
        {
            case SPLASH: StateSplash(); break;
            case SPIRAL: StateSpiral(); break;
            case FLEE: StateFlee(); break;
        }
    }

    private void StateFlee()
    {
        float tempSpeed = 0;
        tempSpeed = (float) (speed + -output/2);
        
        var position = transform.position;
        
        position = Vector2.MoveTowards( new Vector2(position.x, position.y), 
            new Vector2(player.position.x, position.y) , -tempSpeed * Time.deltaTime);
        transform.position = position;
        //Debug.Log("Speed: " + tempSpeed);
    }

    private void StateSpiral()
    {
        var position = transform.position;
        float tempSpeed = 0;
        if (output > 0)
            tempSpeed = (float) (speed + output);
        if (output <= 0)
            tempSpeed= (float) (speed + -output);
        
        switch (nextWayPoint)
        {
            case 0:         
                position = Vector2.MoveTowards( new Vector2(position.x, position.y), 
                new Vector2(wayPoint[0].position.x, position.y) , tempSpeed * Time.deltaTime); 
                
                if (position.x <= wayPoint[0].position.x)
                    nextWayPoint = 1;
                
                break;
            
            case 1:
                position = Vector2.MoveTowards( new Vector2(position.x, position.y), 
                    new Vector2(wayPoint[1].position.x, position.y) , tempSpeed * Time.deltaTime);
                
                if (position.x >= wayPoint[1].position.x)
                    nextWayPoint = 0;
                
                break;
        }
        if(Time.time >= nextShot)
        {
            nextShot = Time.time + delay;
            SpawnSpiralBullet();
            
        }
        
        transform.position = position;
    }

    public void StateSplash()
    {
        var position = transform.position;
        position = Vector2.MoveTowards( new Vector2(position.x, position.y), 
            new Vector2(player.position.x, position.y) , speed * Time.deltaTime);
        transform.position = position;
        
        
    }
    
    private void SpawnSpreadBullet()
    {
        float angleStep = (endAngle - startAngle) / bulletAmount;
        float angle = startAngle;

        for (int i = 0; i < bulletAmount + 1; i++)
        {
            var position = transform.position;
            float bulDirX = position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float bulDirY = position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3(bulDirX, bulDirY, 0f);
            Vector2 bulDir = (bulMoveVector - position).normalized;

            GameObject b = Instantiate(bullet) as GameObject;
            b.transform.position = position;
            //b.transform.rotation = transform.rotation;
            b.GetComponent<EnemyBullet>().SetMoveDirection(bulDir);
            //b.GetComponent<Rigidbody2D>();
            angle += angleStep;
        }
    }

    private void SpawnSpiralBullet()
    {
        for(int i=0;i<=1;i++)
        {
            var position = transform.position;
            float bulDirX=position.x+Mathf.Sin(((angle+120f*i)*Mathf.PI)/120f);
            float bulDirY=position.y+Mathf.Cos(((angle+120f*i)*Mathf.PI)/120f);

            Vector3 bulMoveVector = new Vector3(bulDirX,bulDirY,0f);
            Vector2 bulDir = (bulMoveVector - position).normalized;

            GameObject b = Instantiate(bullet) as GameObject;
            b.transform.position=position;
            b.transform.rotation=transform.rotation;
            b.GetComponent<EnemyBullet>().SetMoveDirection(bulDir);
            b.GetComponent<EnemyBullet>().moveSpeed = 5f;

        }
        angle +=10f;
        if(angle>=360f)
        {
            angle=0f;
        }
        //Debug.Log("DoubleFire");
    }
    
    private void SpawnMinion()
    {
        GameObject m = Instantiate(minion);
        m.transform.position = transform.position;
    }
}

