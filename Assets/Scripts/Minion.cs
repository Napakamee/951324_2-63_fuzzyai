﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Minion : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField]private Vector3 playerLocation;
    public float minSpeed = 2, maxSpeed = 4, randSpeed;
    public float hp = 2;
    
    [SerializeField] private GameObject explode;
    [SerializeField] private GameObject bigExplode;
    
    public float speed = 3.0f;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        randSpeed = Random.Range(minSpeed, maxSpeed);
        speed = randSpeed;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerLocation = player.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position,
            new Vector2(playerLocation.x, -6.5f),
            speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            ContactPoint2D contact = other.contacts[0];
            Vector3 pos = contact.point;
            
            hp -= GameManager.Instance.bulletDamage;
            GameObject expl = Instantiate(explode, pos, Quaternion.identity) as GameObject;
            Destroy(expl, .5f);
            if (hp <= 0)
            {
                GameObject bigExpl = Instantiate(bigExplode, pos, Quaternion.identity) as GameObject;
                Destroy(bigExpl, 1f);
                Destroy(this.gameObject);
            }
            Destroy(other.gameObject);
        }
    }
}
