﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fuzzy_Logic_Sugeno
{
    class FuzzyLogic
    {

        public double FuzzyGrade(double value, double x0, double x1)
        {
            double result = 0;
            double x = value;
            if (x <= x0)
                result = 0;
            else if (x >= x1)
                result = 1;
            else
                result = (x / (x1 - x0)) - (x0 / (x1 - x0));
            return result;
        }
        public double FuzzyReverseGrade(double value, double x0, double x1)
        {
            double result = 0;
            double x = value;
            if (x <= x0)
                result = 1;
            else if (x >= x1)
                result = 0;
            else
                result = (-x / (x1 - x0)) + (x1 / (x1 - x0));
            return result;
        }
        public double FuzzyTriangle(double value, double x0, double x1, double x2)
        {
            double result = 0;
            double x = value;
            if (x <= x0)
                result = 0;
            else if (x == x1)
                result = 1;
            else if (x >= x2)
                result = 0;
            else if ((x > x0) && (x < x1))
                result = (x / (x1 - x0)) - (x0 / (x1 - x0));
            else
                result = (-x / (x2 - x1)) + (x2 / (x2 - x1));
            return result;
        }
        public double FuzzyTrapezoid(double value, double x0, double x1, double x2, double x3)
        {
            double result = 0;
            double x = value;
            if (x <= x0)
                result = 0;
            else if ((x >= x1) && (x <= x2))
                result = 1;
            else if ((x > x0) && (x < x1))
                result = (x / (x1 - x0)) - (x0 / (x1 - x0));
            else
                result = (-x / (x3 - x2)) + (x3 / (x3 - x2));
            return result;
        }

        public double FuzzyAND(double A, double B)
        {
            return Math.Min(A, B);
        }

        public double FuzzyOR(double A, double B)
        {
            return Math.Max(A, B);
        }

        public double FuzzyNOT(double A)
        {
            return 1.0 - A;
        }





    }
}
